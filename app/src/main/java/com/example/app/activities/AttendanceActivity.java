package com.example.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.adapters.SchoolAttendanceAdapter;
import com.example.app.models.Class;
import com.example.app.models.School;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


//import org.parceler.Parcels;

import java.lang.reflect.Type;

public class AttendanceActivity extends AppCompatActivity {

    private Class mSelectedClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        Gson gson = new Gson();
        String classString = getIntent().getStringExtra("selectedclass");
        if (classString != null) {
            Type type = new TypeToken<Class>() {}.getType();
            mSelectedClass = gson.fromJson(classString, type);
        } else {
            Log.v("Exception", "Failed to retrieve this class' information");
        }

        //Class selectedClass = Parcels.unwrap(getIntent().getParcelableExtra("selectedclass"));

        TextView className = (TextView) findViewById(R.id.thisclass);
        className.setText(mSelectedClass.getName());

        RecyclerView dailyAttendanceList = (RecyclerView) findViewById(R.id.attendances);
        SchoolAttendanceAdapter adapter = new SchoolAttendanceAdapter(this, mSelectedClass.getCalendars());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        dailyAttendanceList.setLayoutManager(mLayoutManager);
        dailyAttendanceList.setAdapter(adapter);
    }
}
