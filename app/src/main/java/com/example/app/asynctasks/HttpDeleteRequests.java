package com.example.app.asynctasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.EditText;

import com.example.app.interfaces.CallbackListener;
import com.example.app.models.Student;
import com.example.app.util.Pair;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.app.util.Constants.DELETE_STUDENT_PROFILE;


public class HttpDeleteRequests extends AsyncTask<String, Void, Void> {


    private int mRequestCode;
    private CallbackListener mListener;
    private String mParameters;
    private Context mContext;

    public HttpDeleteRequests(String parameters, int requestcode, CallbackListener listener, Context context) {
        mRequestCode = requestcode;
        mListener = listener;
        mParameters = parameters;
        mContext = context;
    }

    @Override
    protected Void doInBackground(String... strings) {
        URL url;
        HttpURLConnection urlConnection;
        OutputStreamWriter outputStream;
        ByteArrayOutputStream arrayOutputStream;
        InputStream inputStream;

        String authorization = "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjVjMjI0NmQwNWQ4NWE0MThmY2NhNzQwMCJ9LCJpYXQiOjE1NDk2NzE0MTR9.e3KZ2F_Rd1AAXO4ofdBxFH2OwIjk-qKVJbBqRSuHRIE";

        try {
            url = new URL(strings[0] + "/" + mParameters);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setRequestProperty("Authorization", authorization);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                switch (mRequestCode) {
                    case DELETE_STUDENT_PROFILE:
                        try {
                            inputStream = new BufferedInputStream(urlConnection.getInputStream());
                            arrayOutputStream = new ByteArrayOutputStream();
                            int bytesread;
                            while ((bytesread = inputStream.read()) != -1) {
                                arrayOutputStream.write(bytesread);   //write the byte to the arrayoutputstream
                            }
                            String deleteStudent = new String(arrayOutputStream.toByteArray(), Charset.defaultCharset());
                            JSONObject requestresult = (JSONObject) new JSONTokener(deleteStudent).nextValue();
                            boolean succ = requestresult.getBoolean("success");
                            mListener.onCompletionHandler(succ, DELETE_STUDENT_PROFILE, null);
                        } catch (Exception e) {e.printStackTrace();}
                        break;
                }
            }


        }catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
